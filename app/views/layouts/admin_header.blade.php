<nav class="navbar navbar-default nav-style">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="navbar-header">
                    <h3 class="navbar-text" href="{{ route('home') }}">Sportquiz Admin</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="navbar-form form-inline">
                        <div class="dropdown navbar-right">
                            <i class="fa fa-user" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer;">
                                {{ Auth::user()->username }}
                                <i class="fa fa-caret-down"></i>
                            </i>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="{{ route('quiz.play') }}">Play Game</a></li>
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
    </div>
</nav>