<div class="side-bar">
    <p class="name-title"> {{ Auth::user()->fullname }}</p>
    <ul class="nav nav-stacked">
        <li @if(Request::url() == route('admin.index')) class="active"@endif><a href="{{ route('admin.index') }}">Dashboard</a></li>
        <li @if(Request::url() == route('category.create')) class="active"@endif><a href="{{ route('category.create') }}">Create Quiz</a></li>
        <li @if(Request::url() == route('category.view')) class="active"@endif><a href="{{ route('category.view') }}">View all categories</a></li>
        <li class=""><a href="#">View Users</a></li>
    </ul>
</div>