<nav class="navbar navbar-default nav-style">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                    <div class="navbar-header">
                        <h3 class="navbar-text" href="{{ route('home') }}">Sportquiz</h3>
                    </div>
            </div>
            <div class="col-md-6">
                @if(!Auth::check())
                <div class="navbar-form form-inline">
                    {{ Form::open(['route'=>'auth.login']) }}
                    <div class="form-group {{ $errors->has('username') ? 'has-error': '' }}">
                        {{ Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Username']) }}
                        {{ $errors->login->first('username', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                        {{ Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password']) }}
                        {{ $errors->login->first('password', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class="form-group">
                        {{ Form::submit('Login', ['class'=>'btn btn-default navbar-button']) }}
                    </div>
                    {{ Form::close()}}
                </div>
                @endif
                    @if(Auth::check())
                        <div class="dropdown navbar-right dropdown-style">
                            <i class="fa fa-user" id="dLabel"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer;">
                                {{ Auth::user()->username }}
                                <i class="fa fa-caret-down"></i>
                            </i>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="{{ route('quiz.play') }}">Play Game</a></li>
                                <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                                @if(Auth::user()->is_admin== true)<li><a href="{{ route('admin.index') }}">Admin Dashboard</a></li>@endif
                            </ul>
                        </div>
                    @endif

            </div>
        </div>
    </div>
</nav>