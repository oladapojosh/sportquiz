<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta name="description" content="">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" >
    <script src="{{ asset('assets/js/jquery-1.11.3.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <title>Sport quiz</title>
</head>
<body>
    <div class="body-wrap">
        @yield('header')
        @yield('content')
    </div>
    @yield('footer')
    @yield('script')
</body>

</html>