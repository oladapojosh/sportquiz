@extends('layouts.master')
@section('header')
    @include('layouts.header')
@endsection
@section('content')
    <div class="banner">
        @include('partials._alert')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="cat-contain">
                            @foreach($category as $categories)
                                <div class="col-md-4">
                                   <a href="{{ route('question', $categories->id) }}"><div class="box">
                                        <p>{{ $categories->category_name }}</p>
                                        <i class="fa fa fa-soccer-ball-o"></i>
                                    </div></a>
                                </div>
                            @endforeach

                        </div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('footer')


@endsection