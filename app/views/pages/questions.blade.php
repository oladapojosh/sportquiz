@extends('layouts.master')
@section('header')
    @include('layouts.header')
@endsection
@section('content')
    <div class="banner">
        @include('partials._alert')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat-contain">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 id="elapse"></h2>
                                        <h2 class="hide_it">Sportquiz<span id="time" class="pull-right"></span></h2> <span class="pull-right"></span>
                                    </div>

                                    <div class="panel-body">
                                        <h4 id="result" class="text-center"></h4>
                                        <p id="test_status" class="text-center"></p>
                                        <h4 id="d_question" class="hide_it"></h4>
                                    </div>

                                    <ul class="list-group hide_it">
                                        <li class="list-group-item" id="choice1"></li>
                                        <li class="list-group-item" id="choice2"> </li>
                                        <li class="list-group-item" id="choice3"> </li>
                                        <li class="list-group-item" id="choice4"> </li>
                                    </ul>
                                </div>

                        <button id="submit" class="btn btn-default hide_it" onclick="checkAnswer();">Submit</button>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="board">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="list-group">
                        <h2 class="board-header text-center">Leader Board</h2>
                            <h4 class="list-group-item-heading">Name <span class="pull-right">Score</span></h4>
                        @if($users!=null)
                            @foreach($users as $user)
                                <a href="#" class="list-group-item">{{ $user->username }} <span class="pull-right">{{ $user->score }}</span></a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer')


@endsection
@section('script')
    <script>
        var jq = jQuery.noConflict();
            var position= 0,choices, correct= 0, disabled=false;
            var items = {{ $questions }}

            function renderQuestion(){
                if (position >= items.length) {
                    document.getElementById("result").innerHTML = "You got " + correct + " out of " + items.length + " questions";
                    document.getElementById("test_status").innerHTML = "Quiz completed";
                    document.getElementById("elapse").innerHTML = "Time elapsed";
                    jq(".hide_it").css('display', 'none');
                    jq.ajaxSetup({
                        headers: { 'csrftoken' : '{{ csrf_token() }}' }
                    });
                    jq.ajax({
                        type: 'POST',
                        url: '/netaviva/sportquiz/public/quiz/score',
                        data: { score: correct},
                        cache: false,
                        success: function(data){
                            disabled = false;
                            alert('success');
                        },
                        error: function(e){
                            alert(e.message);
                        }
                    });
                    jq('a').click(function(event){
                        if(disabled)
                            jq('a').addClass('disabled');
                            event.preventDefault();
                    });
                    position = 0;
                    correct = 0;
                    return false;
                }
                var question = items[position].question;
                var option1 = items[position].option_1;
                var option2 = items[position].option_2;
                var option3 = items[position].option_3;
                var option4 = items[position].option_4;
                jq("#d_question").html(question);
                jq("#choice1").html('<input type="radio" name="choices" value="'+option1+'"> ' + option1);
                jq("#choice2").html('<input type="radio" name="choices" value="'+option2+'"> ' + option2);
                jq("#choice3").html('<input type="radio" name="choices" value="'+option3+'"> ' + option3);
                jq("#choice4").html('<input type="radio" name="choices" value="'+option4+'"> ' + option4);
            }
            choices = "";
             function checkAnswer() {
                 choices = document.getElementsByName('choices');
                 for (var j = 0;j<= choices.length; j++) {
                     if (choices[j].checked) {
                         var choice = choices[j].value;
                         if (choice == items[position].correct_answer) {
                             correct++;
                         }
                         position++;
                         renderQuestion();
                     }
                 }
             }
            function startTimer(duration, display){
                var start = Date.now(),
                        diff,
                        minutes,
                        seconds;
                function timer(){
                    diff = duration -parseInt(( Date.now() - start)/ 1000);
                    // does the same job as parseInt truncates the float
                    minutes = parseInt((diff/60));
                    seconds = parseInt((diff % 60));

                    minutes = minutes < 1 ? "0" + minutes : minutes;
                    seconds = seconds < 1 ? "0" + seconds : seconds;
                    if(minutes < 1 && seconds < 1){
                        clearTimeout(timer);
                        display = document.getElementById('time').innerHTML = 'Time Out';
                        jq(".hide_it").css('display', 'none');
                        document.getElementById("result").innerHTML = "You got " + correct + " out of " + items.length + " questions";
                        document.getElementById("test_status").innerHTML = "Quiz completed";
                        document.getElementById("elapse").innerHTML = "Time elapsed";
                        jq.ajaxSetup({
                            headers: { 'csrftoken' : '{{ csrf_token() }}' }
                        });
                        jq.ajax({
                            type: 'POST',
                            url: '/netaviva/sportquiz/public/quiz/score',
                            data: { score: correct},
                            cache: false,
                            success: function(data){
                                disabled=false;
                                alert('success');
                            },
                            error: function(e){
                                alert(e.message);
                            }
                        });
                        jq('a').click(function(event){
                            if(disabled)
                                jq('a').addClass('disabled');
                                event.preventDefault();
                        });
                        position = 0;
                        correct = 0;
                        return false;
                    }
                    display.textContent = minutes + ':' + seconds;

                    if(diff < 0){
                        start = Date.now() + 1000;
                    }
                };

                timer();
                setInterval(timer,1000);

            }

        window.onload = function () {
            var tenMinutes = {{ $quizLimit }}
                    display = document.querySelector('#time');
            startTimer(tenMinutes, display);
        };
        window.addEventListener("load", renderQuestion, false);

    </script>
@stop