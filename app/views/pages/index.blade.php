@extends('layouts.master')
@section('header')
    @include('layouts.header')
@endsection
@section('content')
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="banner-text">
                    <h1>Take Quiz, Earn points</h1>
                    <h3>Get Airtime</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="auth-panel animated fadeInRight">
                    <div class="login-box">

                    </div>
                    @include('partials._alert')
                    <div class="register-box">
                        {{ Form::open(['route'=>'auth.register']) }}
                        <div class="form-group {{ $errors->has('fullname') ? 'has-error': '' }}">
                            {{ Form::text('fullname', null, ['class'=>'form-control input-box', 'placeholder'=>'Your fullname']) }}
                            {{ $errors->register->first('fullname', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('username') ? 'has-error': '' }}">
                            {{ Form::text('username', null, ['class'=>'form-control input-box', 'placeholder'=>'Username']) }}
                            {{ $errors->register->first('username', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                            {{ Form::text('email', null, ['class'=>'form-control input-box', 'placeholder'=>'Email']) }}
                            {{ $errors->register->first('email', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                            {{ Form::password('password', ['class'=>'form-control input-box', 'placeholder'=>'Password']) }}
                            {{ $errors->register->first('password', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                            {{ Form::password('password_confirmation', ['class'=>'form-control input-box', 'placeholder'=>'Confirm your password']) }}
                            {{ $errors->register->first('password_confirmation', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Register', ['class'=>'btn btn-default btn-lg btn-block register-btn-style']) }}
                        </div>
                        {{ Form::close()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('footer')


@endsection