@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.admin-sidebar')
            </div>
            <div class="col-md-9">
                @include('partials._alert')
                <div class="all">
                    <div class="page-header">
                        <h3>Edit questions <small>edit questions in {{ $category->category_name }} category</small></h3>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-2">
                            {{ Form::model($question,['route'=>['question.update',$category->category_name, $question->id], 'method'=>'patch']) }}
                            <div class="form-group">
                                {{ Form::text('question', null, ['class'=>'form-control', 'placeholder'=>'Your Question']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('option_1', null, ['class'=>'form-control', 'placeholder'=>'Option 1']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('option_2', null, ['class'=>'form-control', 'placeholder'=>'Option 2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('option_3', null, ['class'=>'form-control', 'placeholder'=>'Option 3']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('option_4', null, ['class'=>'form-control', 'placeholder'=>'Option 4']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('correct_answer', null, ['class'=>'form-control', 'placeholder'=>'Correct Answer']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')


@endsection