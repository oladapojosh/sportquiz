@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.admin-sidebar')
            </div>
            <div class="col-md-9">
                @include('partials._alert')
                <div class="all">
                    <div class="page-header">
                        <h3>All questions <small>all questions in this category</small></h3>

                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Questions
                                </div>
                                @if($questions->count() == 0)
                                    <div class="panel-body">
                                        <p>No question found</p>
                                    </div>
                                @elseif($questions->count()>0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="5%" class="text-center">#</th>
                                            <th width="40%" class="text-center">Question</th>
                                            <th width="40%" class="text-center">Correct Answer</th>
                                            <th width="15%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($questions as $question)
                                            <tr>
                                                <td width="5%" class="text-center">{{ $question->id }}</td>
                                                <td width="40%" class="text-center">{{ $question->question }}</td>
                                                <td width="40%" class="text-center">{{ $question->correct_answer }}</td>
                                                <td width="15%" class="text-center">
                                                    <a href="{{ route('question.edit',[$question->category->category_name,$question->id]) }}" title="edit question"><button class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button></a>
                                                    <a href="{{ route('question.delete', $question->id) }}" title="delete question"><button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')


@endsection