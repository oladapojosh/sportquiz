@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.admin-sidebar')
            </div>
            <div class="col-md-9">
                @include('partials._alert')
                <div class="all">
                    <div class="page-header">
                        <h3>View Categoies <small>see all categories created</small></h3>

                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    All categories
                                </div>
                                @if($categories->count() == 0)
                                    <div class="panel-body">
                                        <p>No category found</p>
                                    </div>
                                @elseif($categories->count() > 0)
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="20%" class="text-center">#</th>
                                            <th width="60%" class="text-center">Category</th>
                                            <th width="20%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td width="20%" class="text-center">{{ $category->id }}</td>
                                                <td width="60%" class="text-center">{{ $category->category_name }}
                                                <td width="20%" class="text-center">
                                                    <a href="{{ route('question.show', $category->id) }}" title="see questions for category"><button class="btn btn-primary btn-xs"><i class="fa fa-check"></i></button></a>
                                                    <a href="{{ route('category.edit', $category->id) }}" title="edit category"><button class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button></a>
                                                    <a href="{{ route('category.delete', $category->id) }}" title="delete category"><button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')


@endsection