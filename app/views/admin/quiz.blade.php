@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.admin-sidebar')
            </div>
            <div class="col-md-9">
                @include('partials._alert')
                <div class="all">
                    <div class="page-header">
                        <h3>Create Quiz <small>your quiz</small></h3>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-2">
                            {{ Form::open(['route'=>'category.store']) }}
                            <div class="form-group">

                                    Category name:
                                    {{ Form::text('category_name', null, ['class'=>'form-control', 'placeholder'=>'Category name']) }}

                            </div>
                            <div class="form-group">

                                Time Limit for quiz in seconds:
                                {{ Form::number('time', null, ['class'=>'form-control']) }}

                            </div>
                            <div class="form-group">

                                    {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="col-md-6 col-md-offset-2">
                            <p>or you can choose from a list of existing categories:</p>
                            <div class="dropdown">
                                <button class="btn btn-primary btn-lg" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Choose Category
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    @foreach($category as $categories)
                                    <li><a href="{{ route('question.create', $categories->id) }}">{{ $categories->category_name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')


@endsection