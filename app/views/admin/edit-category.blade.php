@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                @include('layouts.admin-sidebar')
            </div>
            <div class="col-md-9">
                @include('partials._alert')
                <div class="all">
                    <div class="page-header">
                        <h3>Edit Category<small> change the name of your category</small></h3>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-2">
                            {{ Form::model($category, ['route'=>['category.update',$category->id], 'method'=>'PATCH']) }}
                            <div class="form-group">
                                Category name:
                                {{ Form::text('category_name', null, ['class'=>'form-control', 'placeholder'=>'Category name']) }}

                            </div>
                            <div class="form-group">

                                Time Limit for quiz in seconds:
                                {{ Form::number('time', null, ['class'=>'form-control']) }}

                            </div>
                            <div class="form-group">

                                {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')


@endsection