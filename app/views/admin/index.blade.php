@extends('layouts.master')
@section('header')
    @include('layouts.admin_header')
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            @include('layouts.admin-sidebar')
        </div>
        <div class="col-md-9">
            <div class="all">
                <div class="page-header">
                    <h3>DashBoard <small>your dashboard</small></h3>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')


@endsection