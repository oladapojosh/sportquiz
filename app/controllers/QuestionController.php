<?php

class QuestionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($category_id)
	{
        /*$questions = Question::where('category_id', '=', $category_id)->orderBy(DB::raw('RAND()'))->simplePaginate(1)*/;
        $category = Category::where('id', '=', $category_id)->first();
        $quizLimit = $category->time;
        $questions = Question::where('category_id', '=', $category_id)->orderBy(DB::raw('RAND()'))->take(4)->get()->toJson();
        $users = User::orderBy('score', 'desc')->get();
        return View::make('pages.questions',compact('category_id','users'))->withQuestions($questions)->with('quizLimit', $quizLimit);
	}
    public function acceptScore(){
        $user_id = Auth::user()->id;
        $newScore = Input::get('score');
        $user = User::where('id', '=',$user_id)->first();
        if($user->count()!=0){
            $oldScore = $user->score;
            $user->score = $oldScore + $newScore;
            $user->save();
            return Redirect::route('quiz.play')->withMessage('You just finished a quiz, Do you want to play another?');
        }
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($category_id)
	{
        $category = Category::find($category_id);
		return View::make('admin.question')->withCategory($category);
	}
    public function viewQuestions($category_id){
        $questions = Question::where('category_id', '=', $category_id)->orderBy('id', 'asc')->get();
        return View::make('admin.show-questions', compact('questions'));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($category_id)
	{
		$input = [
			'question'=> Input::get('question'),
            'option1' => Input::get('option_1'),
            'option2'=> Input::get('option_2'),
            'option3'=> Input::get('option_3'),
            'option4'=> Input::get('option_4'),
            'correct_answer'=> Input::get('correct_answer'),
		];

        $rules = [
            'question'=> 'required',
            'option1'=>'required',
            'option2'=>'required',
            'option3'=>'required',
            'option4'=>'required',
            'correct_answer'=> 'required',
        ];
        $validator = Validator::make($input, $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($validator->passes()){
            $category = Category::find($category_id);
            $question = new Question();
            $question->question = $input['question'];
            $question->option_1 = $input['option1'];
            $question->option_2 = $input['option2'];
            $question->option_3 = $input['option3'];
            $question->option_4 = $input['option4'];
            $question->correct_answer = $input['correct_answer'];
            $success = $category->question()->save($question);
            if($success){
                return Redirect::back()->withMessage('Question successfully created!');
            }else{
                return Redirect::back()->withErrors('An Error Occurred, please try again!')->withInput();
            }
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function editQuestion($category_name,$question_id)
	{
        $category = Category::where('category_name', '=', $category_name)->first();
		$question = Question::find($question_id);
        return View::make('admin.edit-question')->withQuestion($question)->withCategory($category);
	}

	public function update($category_name, $question_id)
	{
        $category = Category::find($category_name);
        $question = Question::find($question_id);
        $input = [
            'question'=> Input::get('question'),
            'option1' => Input::get('option_1'),
            'option2'=> Input::get('option_2'),
            'option3'=> Input::get('option_3'),
            'option4'=> Input::get('option_4'),
            'correct_answer'=> Input::get('correct_answer'),
        ];

        $rules = [
            'question'=> 'required',
            'option1'=>'required',
            'option2'=>'required',
            'option3'=>'required',
            'option4'=>'required',
            'correct_answer'=> 'required',
        ];
        $validator = Validator::make($input, $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($validator->passes()){
            $question->question = $input['question'];
            $question->option_1 = $input['option1'];
            $question->option_2 = $input['option2'];
            $question->option_3 = $input['option3'];
            $question->option_4 = $input['option4'];
            $question->correct_answer = $input['correct_answer'];
            $question->save();
            $update = $question->category->save();
            if($update){
                return Redirect::back()->withMessage('Question successfully updated');
            }else{
                return Redirect::back()->withErrors('An Error Occurred, please try again!')->withInput();
            }
        }
	}

	public function deleteQuestion($question_id)
	{
		$question = Question::find($question_id);
        $question->delete();
        return Redirect::route('question.show')->withMessage('Question has been deleted!');
	}


}
