<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$category = Category::orderBy('id','asc')->get();
		return View::make('pages.play', compact('category'));
	}
    public function viewCategory(){
        $categories = Category::orderBy('id', 'asc')->get();
        return View::make('admin.categories', compact('categories'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $category = Category::orderBy('id', 'asc')->get();
        return View::make('admin.quiz',compact('category'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = [
            'category_name'=> Input::get('category_name'),
            'time' => Input::get('time'),
        ];

        $rule = [
            'category_name'=> 'required|unique:category,category_name',
            'time'=>'required',
        ];
        $validator = Validator::make($input, $rule);

        if($validator->fails()){
            return Redirect::route('category.create')->withErrors($validator);
        }
        if($validator->passes()){
            $category_create = Category::create([
                'category_name'=>$input['category_name'],
                'time'=>$input['time'],
            ]);
            if($category_create){
				$category = Category::where('category_name', '=', $input['category_name'])->first();
                if (!empty($category)) {
                    return Redirect::route('question.create', $category->id)->withSuccess('Category successfully created');
                }
            }
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editCategory($id)
	{
        $category = Category::find($id);
        return View::make('admin.edit-category')->withCategory($category);

	}



	public function update($id)
	{
        $category = Category::find($id);
        $input = [
            'category_name'=> Input::get('category_name'),
            'time'=> Input::get('time'),
        ];

        $rule = [
            'category_name'=> 'required|unique:category,category_name',
            'time'=>'required',
        ];
        $validator = Validator::make($input, $rule);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }
        if($validator->passes()){
            $category->category_name = $input['category_name'];
            $category->time =$input['time'];
            $update = $category->save();
            if($update){
                return Redirect::back()->withSuccess('Category successfully updated');
            }
        }
	}


	public function deleteCategory($id)
	{
		$category = Category::find($id);
        $category->delete();
        return Redirect::route('category.view')->withMessage('Category has been deleted!');
	}


}
