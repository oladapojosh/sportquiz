<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AuthController extends BaseController {


	public function register()
	{
		if(Request::isMethod('post')){
            $input = [
                'username'=> Input::get('username'),
                'fullname'=> Input::get('fullname'),
                'email'=> Input::get('email'),
                'password'=>Input::get('password'),
                'password_confirmation'=> Input::get('password_confirmation')
            ];
            $rules = [
                'username'=> 'required|unique:users,username',
                'fullname'=> 'required',
                'email'=> 'required|unique:users,email',
                'password'=>'required|confirmed|min:8',
                'password_confirmation'=> 'required|min:8'
            ];

            $validate = Validator::make($input, $rules);

            if($validate->passes()){
                $user = User::create([
                    'fullname'=> $input['fullname'],
                    'username' => $input['username'],
                    'email'=> $input['email'],
                    'password'=> Hash::make($input['password']),
                ]);
                if($user){
                    Auth::login($user);
                    return Redirect::route('home')->withSuccess('You are signed in!');
                }else{
                    return Redirect::route('home')->withErrors("We could create your account, please try again.")->withInput();
                }
            }else{
                return Redirect::route('home')->withErrors($validate, 'register')->withInput();
            }
		}
	}

    public function login(){
        if(Request::isMethod('post')){
            $data = Input::only(['username', 'password']);

            $rule = [
                'username' => 'required',
                'password' => 'required|min:8'
            ];
            $validate = Validator::make($data, $rule);

            if($validate->fails()){
                return Redirect::route('home')->withErrors($validate, 'login')->withInput();
            }
            if($validate->passes()){
                if(Auth::attempt($data, true)){
                    return Redirect::route('home')->withMessage('Welcome, you are now signed in!');
                }else{
                    return Redirect::route('home')->withErrors('Email/password is incorrect!')->withInput();
                }
            }
        }
    }
    public function logout(){
        Auth::logout();
        return Redirect::route('home')->withSuccess('You are logged out!');
    }




}
