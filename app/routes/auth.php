<?php
Route::group(['prefix'=>'auth'], function(){
   Route::match(['GET', 'POST'], '/register', [
       'uses'=>'AuthController@register',
       'as'=>'auth.register',
   ]);
    Route::match(['GET', 'POST'], '/login', [
        'uses'=>'AuthController@login',
        'as'=>'auth.login',
    ]);
    Route::match(['GET'], '/logout', [
        'as'=> 'auth.logout',
        'uses'=>'AuthController@logout'
    ]);

});