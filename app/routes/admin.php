<?php
Route::group(['prefix'=>'admin', 'before'=>'auth|admin'], function(){

   Route::match(['GET', 'POST'], '/index', [
      'uses'=>'AdminController@index',
       'as'=>'admin.index',
   ]);
    Route::match(['GET', 'POST'], '/create-category', [
        'uses'=>'CategoryController@create',
        'as'=>'category.create',

    ]);
    Route::match(['GET', 'POST'], '/categories', [
        'uses'=>'CategoryController@viewCategory',
        'as'=>'category.view',

    ]);

    Route::match(['GET', 'POST'], '/{id}/delete-category', [
        'uses'=>'CategoryController@deleteCategory',
        'as'=>'category.delete',
    ]);
    Route::match(['GET'], '/{id}/edit-category', [
        'uses'=>'CategoryController@editCategory',
        'as'=>'category.edit',
    ]);
    Route::match(['PATCH'], '/{id}/update-category', [
        'uses'=>'CategoryController@update',
        'as'=>'category.update',
    ]);
    Route::match(['POST'], '/store-category', [
        'uses'=>'CategoryController@store',
        'as'=>'category.store',
    ]);

    Route::match(['GET', 'POST'], '{id}/create-question', [
        'uses'=>'QuestionController@create',
        'as'=>'question.create',
    ]);
    Route::match(['GET', 'POST'], '{id}/store-question', [
        'uses'=>'QuestionController@store',
        'as'=>'question.store',
    ]);
    Route::match(['GET'], '/{id}/view-questions', [
       'uses'=>'QuestionController@viewQuestions',
        'as'=>'question.show',
    ]);
    Route::match(['GET', 'POST'], '/{question_id}/delete-question', [
        'uses'=>'QuestionController@deleteQuestion',
        'as'=>'question.delete',
    ]);
    Route::match(['GET'], '{category}/{question_id}/edit-question', [
        'uses'=>'QuestionController@editQuestion',
        'as'=>'question.edit',
    ]);
    Route::match(['PATCH'], '{category}/{question_id}/update-question', [
        'uses'=>'QuestionController@update',
        'as'=>'question.update',
    ]);

});