<?php
Route::group(['prefix'=>'quiz', 'before'=>'auth'], function(){

    Route::match(['GET', 'POST'], '/play', [
       'uses'=>'CategoryController@index',
        'as'=>'quiz.play',
    ]);
    Route::match(['GET', 'POST'], '/{id}/questions', [
        'uses'=>'QuestionController@index',
        'as'=>'question'
    ]);
    Route::match(['GET', 'POST'], '/score', [
        'uses'=>'QuestionController@acceptScore',
        'as'=>'score'
    ]);
});

/*Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});*/
