<?php

Route::match(['GET'], '/', [
    'as'=>'home',
    'uses'=>'PageController@index'
]);
