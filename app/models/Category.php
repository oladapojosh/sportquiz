<?php
class Category extends Eloquent{

    protected $table ='category';

    protected $fillable = [
        'category_name',
        'time'
    ];

    public function question(){
        return $this->hasMany('Question');
    }
}