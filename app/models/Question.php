<?php

class Question extends Eloquent{

    protected $table = 'question';

    protected $fillable = [
        'question',
        'option_1',
        'option_2',
        'option_3',
        'option_4',
        'correct_answer',
    ];

    public function category(){
        return $this->belongsTo('Category', 'category_id');
    }

}